;;General config for ORG mode etc

(provide 'dotconfig)

;;Get Ditaa
(setq org-ditaa-jar-path "/usr/local/share/ditaa0_9.jar")

(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (ditaa . t)
   (C . t)
;;   (cpp . t)
;;   (sh . t)
))


;; FLyspell Shizzle
(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)
(autoload 'flyspell-delay-command "flyspell" "Delay on command." t)
(autoload 'tex-mode-flyspell-verify "flyspell" "" t) 
(add-hook 'LaTeX-mode-hook (lambda() (flyspell-mode 1)))

