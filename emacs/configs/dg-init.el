;; ------- Initialise and setup custom vars for emacs
(provide 'dg-init)


;; Variable to hold details of packages to install
(defvar required-packages
  '()
)

