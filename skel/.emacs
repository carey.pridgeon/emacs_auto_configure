;; .emacs

;;  ----------- COPY THIS IF YOU WANT PACKAGE MAGIC -------------
;;Load some global setup stuff
(add-to-list 'load-path "/usr/share/emacs/configs")
;;Initialise global variables
(require 'dg-init)

;;Setup Package Library 
(package-initialize)


;;Load The rest of the cofigurrations
(require 'org)
(require 'dg-package)
(require 'dg-general)
(require 'dg-org)

;; ------------ YOUR CONFIG CAN GO HERE ---------------


(load-theme 'light-blue t)
;;(set-frame-parameter nil 'background-mode 'dark)
(enable-theme 'light-blue)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(diff-switches "-u")
 )


;;; uncomment for CJK utf-8 support for non-Asian users
;; (require 'un-define)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
