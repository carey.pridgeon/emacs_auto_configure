This setup was created by Dr Daniel Goldsmith of Coventry University. I'm just sharing it because it is rather great.
to make sure you have everything needed installed create a new user.

If all is well this will populate their account with the custom .emacs. 

You need at least, on Ubuntu:

ditaa0_9.jar in /usr/local/share

texlive, texlive-latex-extra and texi2dvi (texinfo)

the emacs folder goes in /usr/share/ where there should already be an emacs folder.

The .emacs in skel goes in /etc/skel/

Then try exporting org files as pdfs or other output targets with various babel blocks from the created user account, installing what's missing (if needed) till it works. 

We use this on Centos, but on Ubuntu this is far simpler to set up. It should work on any Linux/Unix.

I personally use it (with paths changed for file locations for simplicity) on MacOS too.